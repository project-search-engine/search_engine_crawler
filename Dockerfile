FROM alpine:3.7
RUN apk add --no-cache python3 \
    && rm -rf /var/cache/apk/*
WORKDIR /search_engine_crawler
COPY requirements.txt /search_engine_crawler
RUN pip3 install -r requirements.txt
COPY . /search_engine_crawler
ENV INITIAL_URL=localhost
CMD python3 -u crawler/crawler.py "$INITIAL_URL"
